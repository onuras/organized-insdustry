extends Control


var MONEY = 0.0




func _ready():
	pass

func _process(delta):
	pass


func big_num_to_str(big_number):
	var n = big_number
	var n_str = String(round(n))
	if n < 1000000000000000:
		var insert_comma_position = n_str.length()
		while insert_comma_position > 0:
			n_str = n_str.insert(insert_comma_position, ",")
			insert_comma_position -= 3
		return n_str

	var total_digit_count = floor((log(n) / log(10)) + 1)
	var show_n_digits = (fmod(total_digit_count, 3)) + 1
	var first_numbers = n_str.left(show_n_digits)

	var N = (int(total_digit_count) / 3) - 1

	return "%s %s" % [ first_numbers, latin_suffix(N) ]


func latin_suffix(n):

	if n == 1:
		return "million"
	elif n == 2:
		return "billion"
	elif n == 3:
		return "trillion"
	elif n == 4:
		return "quadrillion"
	elif n == 5:
		return "quintillion"
	elif n == 6:
		return "sextillion"
	elif n == 7:
		return "septillion"
	elif n == 8:
		return "octillion"
	elif n == 9:
		return "nonillion"
	elif n == 10:
		return "decillion"


	var suffixes = [
		[ "un", "deci", "centi" ],
		[ "duo", "viginti", "ducenti" ],
		[ "tre", "triginta", "trecenti" ],
		[ "quattuor", "quadraginta", "quadringenti" ],
		[ "quinqua", "quinquaginta", "quingenti" ],
		[ "se", "sexaginta", "sescenti" ],
		[ "septe", "septuaginta", "septingenti" ],
		[ "octo", "octoginta", "octingenti" ],
		[ "nove", "nonaginta", "nongenti" ],
	]

	var digits_str = String(n)
	var digits = []
	for d in digits_str:
		digits.push_front(int(d))

	var tens = 0
	var suffix = ""
	for i in digits:
		if i != 0:
			suffix += suffixes[i-1][tens]
		tens += 1

	suffix += "llion"
	return suffix
