# Organized Industry



## Design Notes

<https://en.wikipedia.org/wiki/Bulk_cargo>

* Raw Materials:
  * Wood
  * Coal
  * Iron
  * Cotton
  * Food
  * Sand and Gravel
  * Petroleum
  * Natural gas


* Buildings
  * Timbermill: Log (Wood)
  * Coal Mine: Coal
  * Iron Mine
  * Textile Factory (Workshop): Cloth
  * Food Factory
  * Asphalt Batch Mix Plant: Asphalt
  * Cement Mill: Cement
  * Oil Refinery: Gasoline
